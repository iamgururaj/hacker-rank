"""A left rotation operation on an array of size  shifts each
of the array's elements  unit to the left.
For example, if left rotations are performed on array , then
the array would become .

Given an array of  integers and a number, , perform  left
rotations on the array. Then print the updated array as a single
line of space-separated integers.
"""

def array_left_rotation(a, n, k):
    """array_left_rotation.
    
    Return calculation in single line.
    """
    return(a[k % n : n] + a[0 : k % n])
    

n, k = map(int, input().strip().split(' '))
a = list(map(int, input().strip().split(' ')))
answer = array_left_rotation(a, n, k);
print(*answer, sep=' ')
